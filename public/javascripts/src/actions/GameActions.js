"use strict";
import {GAME_ACTIONS} from '../lib/constants';
import fetch from 'node-fetch';
import * as Immutable from 'immutable';


export const movePiece = (pieceId, holderId) => ({
    type: GAME_ACTIONS.MOVE_PIECE,
    pieceId: pieceId,
    holderId: holderId
});

export const resizeBoard = (width, height) => ({
    type: GAME_ACTIONS.RESIZE_BOARD,
    width: width,
    height: height
});

export const rollDice = (values) => ({
    type: GAME_ACTIONS.ROLL_DICE,
    diceValues: values
});

export const rollDiceAsync = () => {

    return (dispatch)  => {
        fetch("http://localhost:3000/api/roll").then(function(res) {
            return res.json();
        }).then(function(json) {
            dispatch(rollDice(Immutable.List(json.values)))
        });
    };
};