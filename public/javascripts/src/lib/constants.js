export const BOARD_WIDTH = 1920;
export const BOARD_HEIGHT = 1080;
export const PIECE_TYPE_LIGHT = 1;
export const PIECE_TYPE_DARK = 2;
export const PIN_WIDTH = 130;
export const PIECE_DIAMETER = 90;


export const GAME_ACTIONS = {
    MOVE_PIECE: "MOVE_PIECE",
    RESIZE_BOARD: "RESIZE_BOARD",
    ROLL_DICE: "ROLL_DICE"
};
