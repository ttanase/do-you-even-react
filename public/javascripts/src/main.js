import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import storeFactory from './store/GameStore';
import {resizeBoard} from './actions/GameActions';
import {Provider} from 'react-redux';

const store = storeFactory();

window.store = store;

window.addEventListener("resize", onResize);

function onResize() {
    store.dispatch(resizeBoard(window.innerWidth, window.innerHeight));
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('react-container')
);
