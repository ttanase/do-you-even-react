"use strict";
import * as Immutable from 'immutable';
import * as Constants from '../lib/constants';
import thunk from 'redux-thunk';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {pieces, boardSize, dice} from '../reducers/reducers';

const initialState = {
    pieces: getInitialPieces(),
    boardSize: {
        width: window.innerWidth,
        height: window.innerHeight
    },
    dice: Immutable.List([1, 2])
};


function getInitialPieces() {
    const {PIECE_TYPE_LIGHT, PIECE_TYPE_DARK} = Constants;
    return Immutable.List([
        {
            id: 0,
            type: PIECE_TYPE_LIGHT,
            pin: 0
        },
        {
            id: 1,
            type: PIECE_TYPE_LIGHT,
            pin: 0
        },
        {
            id: 2,
            type: PIECE_TYPE_LIGHT,
            pin: 11
        },
        {
            id: 3,
            type: PIECE_TYPE_LIGHT,
            pin: 11
        },
        {
            id: 4,
            type: PIECE_TYPE_LIGHT,
            pin: 11
        },
        {
            id: 5,
            type: PIECE_TYPE_LIGHT,
            pin: 11
        },
        {
            id: 6,
            type: PIECE_TYPE_LIGHT,
            pin: 11
        },
        {
            id: 7,
            type: PIECE_TYPE_LIGHT,
            pin: 16
        },
        {
            id: 8,
            type: PIECE_TYPE_LIGHT,
            pin: 16
        },
        {
            id: 9,
            type: PIECE_TYPE_LIGHT,
            pin: 16
        },
        {
            id: 10,
            type: PIECE_TYPE_LIGHT,
            pin: 18
        },
        {
            id: 11,
            type: PIECE_TYPE_LIGHT,
            pin: 18
        },
        {
            id: 12,
            type: PIECE_TYPE_LIGHT,
            pin: 18
        },
        {
            id: 13,
            type: PIECE_TYPE_LIGHT,
            pin: 18
        },
        {
            id: 14,
            type: PIECE_TYPE_LIGHT,
            pin: 18
        },
        {
            id: 15,
            type: PIECE_TYPE_DARK,
            pin: 5
        },
        {
            id: 16,
            type: PIECE_TYPE_DARK,
            pin: 5
        },
        {
            id: 17,
            type: PIECE_TYPE_DARK,
            pin: 5
        },
        {
            id: 18,
            type: PIECE_TYPE_DARK,
            pin: 5
        },
        {
            id: 19,
            type: PIECE_TYPE_DARK,
            pin: 5
        },
        {
            id: 20,
            type: PIECE_TYPE_DARK,
            pin: 7
        },
        {
            id: 21,
            type: PIECE_TYPE_DARK,
            pin: 7
        },
        {
            id: 22,
            type: PIECE_TYPE_DARK,
            pin: 7
        },
        {
            id: 23,
            type: PIECE_TYPE_DARK,
            pin: 12
        },
        {
            id: 24,
            type: PIECE_TYPE_DARK,
            pin: 12
        },
        {
            id: 25,
            type: PIECE_TYPE_DARK,
            pin: 12
        },
        {
            id: 26,
            type: PIECE_TYPE_DARK,
            pin: 12
        },
        {
            id: 27,
            type: PIECE_TYPE_DARK,
            pin: 12
        },
        {
            id: 28,
            type: PIECE_TYPE_DARK,
            pin: 23
        },
        {
            id: 29,
            type: PIECE_TYPE_DARK,
            pin: 23
        },
    ]);
}


const storeFactory = (stateData = initialState) =>
    createStore(
        combineReducers({
            pieces,
            boardSize,
            dice
        }),
        stateData,
        applyMiddleware(thunk)
    );

export default storeFactory;
