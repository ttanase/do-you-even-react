import {connect} from 'react-redux';
import {movePiece} from '../../actions/GameActions';
import PieceHoldersList from '../ui/PieceHoldersList';

const Holders = connect(
    null,
    dispatch => ({
        onPieceMove(pieceId, holderId){
            dispatch(movePiece(pieceId, holderId));
        }
    })
)(PieceHoldersList);


export default Holders;