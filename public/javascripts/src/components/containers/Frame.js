import {connect} from 'react-redux';
import Board from '../ui/Board';

const Frame = connect(
    state => ({
        boardSize: state.boardSize,
        diceValues: state.dice,
        pieces: state.pieces
    }),
    null
)(Board);

export default Frame;