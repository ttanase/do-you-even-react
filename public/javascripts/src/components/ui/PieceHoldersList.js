import React from 'react';
import PieceHolder from './PieceHolder';

class PieceHoldersList extends React.Component {

    render(){
        let pieceHolders = [];
        for (let i = 0; i < 24; i++) {
            pieceHolders.push(
                <PieceHolder index={i} key={i} dropHandler={this.props.onPieceMove}/>
            );
        }

        return (<div>{pieceHolders}</div>);
    }
}

PieceHoldersList.propTypes = {
    onPieceMove: React.PropTypes.func
};

export default PieceHoldersList;
