import React from 'react';

class HugeLuckPopup extends React.Component {

    //values,
    //isRolling

    shouldComponentUpdate(nextProps, nextState){

        if(nextProps.isRolling) {
            return false;
        }

        return true;
    }


    render() {


        const isDubla = this.props.values.get(0) == this.props.values.get(1);


        const style = {


            display: (isDubla && !this.props.isRolling ) ? "block" : "none",
            position: "absolute",
            top: 240 - 490,
            left: 560 - 1265,
            background: "url(/images/huge_luck.jpg)",
            width: 800,
            height: 600

        };

        return (
            <div style={style}></div>
        );
    }
}

export default HugeLuckPopup;