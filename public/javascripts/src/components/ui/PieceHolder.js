import React from 'react';
import * as Constants from '../../lib/constants';

class PieceHolder extends React.Component {

    render() {

        let left = 0;
        let top = 0;
        let pin = this.props.index;
        const pinWidth = Constants.PIN_WIDTH;
        const boardHeight = Constants.BOARD_HEIGHT;
        let leftOffset = 0;


        if (pin < 12) { //bottom row
            left = 150 + pinWidth * pin + 60 * ( pin > 5 ? 1 : 0) + leftOffset;
            top = 540
        } else { //top row
            left = 150 + pinWidth * (23 - pin) + 60 * (pin < 18 ? 1 : 0) + leftOffset;
            top = 40;
        }

        var style = {
            width: pinWidth,
            height: 500,
            position: "absolute",
            top: top,
            left: left
        };

        return (
            <div style={style}
                 onDragOver={(event) => this.preventDefault(event)}
                 onDrop={(event) => this.drop(event)}>

            </div>
        );
    }

    preventDefault(event){
        event.preventDefault();
    }

    drop(event){
        event.preventDefault();
        let pieceId = parseInt(event.dataTransfer.getData("pieceId"));
        this.props.dropHandler(pieceId, this.props.index);
    }
}

PieceHolder.propTypes = {
    index: React.PropTypes.number,
    dropHandler: React.PropTypes.func
};

export default PieceHolder;