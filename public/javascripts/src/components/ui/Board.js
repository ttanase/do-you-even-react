import React from 'react';
import * as Constants from '../../lib/constants';
import Holders from '../containers/Holders';
import GamePiece from './GamePiece';
import LuckGenerator from './LuckGenerator';

class Board extends React.Component {

    render() {

        const width = Constants.BOARD_WIDTH;
        const height = Constants.BOARD_HEIGHT;
        const windowWidth = this.props.boardSize.width;
        const windowHeight = this.props.boardSize.height;
        const scaleX = windowWidth / width;
        const scaleY = windowHeight / height;
        const transform = `scale( ${scaleX}, ${scaleY})`;


        //the number of pieces per each pin
        let gameLayout = new Array(26).fill(0);
        const gamePieces = this.props.pieces.map((pieceProperties, id) => {
            const {type, pin} = pieceProperties;
            const position = gameLayout[pin]++;
            return <GamePiece
                key={id}
                id={id}
                type={type}
                pin={pin}
                position={position}
            />;
        });

        return (
            <div className="app-container"
                 style={{
                     width,
                     height,
                     transform,
                 }}>
                <Holders/>
                {gamePieces}
                <LuckGenerator diceValues={this.props.diceValues}/>

            </div>
        );
    }
}

export default Board;
