import React from 'react';
import * as Constants from '../../lib/constants';

class GamePiece extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            left: 0,
            top: 0,
            isDragged: false
        };
    }

    componentWillMount() {
        const {pin, position} = this.props;
        this.setCoordinates(pin, position);
    }

    componentWillReceiveProps(nextProps) {
        this.setCoordinates(nextProps.pin, nextProps.position);
    }

    setCoordinates(pin, position) {

        const type = this.props.type;
        const pinWidth = Constants.PIN_WIDTH;
        const boardHeight = Constants.BOARD_HEIGHT;
        const pieceDiameter = Constants.PIECE_DIAMETER;
        const leftOffset = pinWidth / 2 - pieceDiameter / 2;

        let left = 0;
        let top = 0;

        if (pin > 23) {
            if (type == Constants.PIECE_TYPE_LIGHT) {
                left = 30;
                top = 50 + 0.6 * pieceDiameter * position;
            } else {
                left = 1920 - 30 - pieceDiameter;
                top = boardHeight - ( 40 + pieceDiameter + 0.6 * pieceDiameter * position);
            }

        } else if (pin < 12) { //bottom row
            left = 150 + pinWidth * pin + 60 * ( pin > 5 ? 1 : 0) + leftOffset;
            top = boardHeight - (40 + pieceDiameter * (position + 1));
        } else { //top row
            left = 150 + pinWidth * (23 - pin) + 60 * (pin < 18 ? 1 : 0) + leftOffset;
            top = 40 + pieceDiameter * position;
        }

        this.setState({
            left,
            top
        });
    }

    render() {

        const type = this.props.type;
        const {top, left, isDragged} = this.state;
        const transform = `translate3d(${left}px, ${top}px, 0)`;

        let typeClass = "";
        switch (type) {
            case Constants.PIECE_TYPE_LIGHT:
                typeClass = "light";
                break;
            case Constants.PIECE_TYPE_DARK:
                typeClass = "dark";
                break;
        }

        const style = {
            transform
        };

        if (isDragged) {
            style.zIndex = 10;
        }

        const className = `piece ${typeClass}`;
        return (
            <div className={className}
                 style={style}
                 draggable="true"
                 onDragStart={event => this.onDragStart(event)}
                 //onDrag={event => this.onDrag(event)}
            >

            </div>
        );
    }

    onDragStart(event) {

        //Send the id to the drop element
        const {id} = this.props;
        event.dataTransfer.setData("pieceId", id);

        //Disable the ugly ghost drag image
        // const dragImage = document.createElement("img");
        // dragImage.style.opacity = "0";
        // event.dataTransfer.setDragImage(dragImage, 0, 0);
    }

    onDrag(event) {

        //Follow the cursor
        const {clientX, clientY} = event;
        const top = clientY * Constants.BOARD_HEIGHT / window.innerHeight - Constants.PIECE_DIAMETER / 2;
        const left = clientX * Constants.BOARD_WIDTH / window.innerWidth - Constants.PIECE_DIAMETER / 2;

        this.setState({
            top: top,
            left: left
        });
    }

    // onDragEnd(event) {
    //     console.debug("On drag end");
    //
    //     this.setState({
    //         isDragged: false
    //     });
    //     this.props.removePieceHandler(this.props.id);
    // }
}

GamePiece.propTypes = {
    id: React.PropTypes.number,
    type: React.PropTypes.oneOf([Constants.PIECE_TYPE_LIGHT, Constants.PIECE_TYPE_DARK]),
    pin: React.PropTypes.number,
    position: React.PropTypes.number
};

export default GamePiece;