import React from 'react';

class Dice extends React.Component {

    render() {

        const className = `dice sprite-dice${this.props.value}`;
        const left = this.props.id * 120;

        return (
            <div className={className} style={{left}}></div>
        );
    }
}

Dice.PropTypes = {
    id: React.PropTypes.number.isRequired,
    value: React.PropTypes.number.isRequired
};

export default Dice;