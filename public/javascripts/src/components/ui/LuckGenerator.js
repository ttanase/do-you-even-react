import React from 'react';
import * as Immutable from 'immutable';
import Dice from './Dice';
import Helper from '../../lib/helper';
import HugeLuckPopup from './HugeLuckPopup';
import {rollDice, rollDiceAsync} from '../../actions/GameActions.js';

class LuckGenerator extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isRolling: false,
            tempValues: Immutable.List([6, 5])
        };
    }

    render() {

        let displayValues = this.props.diceValues;
        if (this.state.isRolling) {
            displayValues = this.state.tempValues;
        }

        const dices = displayValues.map((value, index) =>
                <Dice key={index} id={index} value={value}/>
        );
        return (
            <div className="luck-generator" onClick={() => this.roll()}>
                {dices}
                <HugeLuckPopup
                    values={displayValues}
                    isRolling={this.state.isRolling}
                    />
            </div>
        );
    }

    roll() {
        console.debug("Rolling...");

        if (!this.state.isRolling) {
            this.setState({
                isRolling: true
            });

            const numberOfSwitches = 2;
            const switchDelay = 200; //ms

            for (let i = 0; i < numberOfSwitches; i++) {
                setTimeout(() => this.setRandomValues(), i * switchDelay);
            }

            //reset rolling state
            setTimeout(() => {
                this.setState({
                    isRolling: false
                });

                store.dispatch(rollDiceAsync());



            }, numberOfSwitches * switchDelay);
        }
    }

    setRandomValues() {
        this.setState({
            tempValues: Immutable.List([Helper.getRandomDiceValue(), Helper.getRandomDiceValue()])
        });
    }
}

LuckGenerator.propTypes = {
    diceValues: React.PropTypes.object.isRequired
};

export default LuckGenerator;