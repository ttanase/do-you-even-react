import {GAME_ACTIONS} from '../lib/constants';
import * as Immutable from 'immutable';

export const boardSize = (state = {}, action) => {
    switch (action.type) {
        case GAME_ACTIONS.RESIZE_BOARD:
            return {
                width: action.width,
                height: action.height
            };
            break;
        default:
            return state;
    }
};

export const piece = (state = {}, action) => {

    switch (action.type) {
        case GAME_ACTIONS.MOVE_PIECE:
            return (state.id !== action.pieceId) ? state :
            {
                id: state.id,
                type: state.type,
                pin: action.holderId
            };
            break;
        default:
            //a reducer MUST return something
            return state;
    }
};

export const pieces = (state = Immutable.List([]), action) => {
    switch (action.type) {
        case GAME_ACTIONS.MOVE_PIECE:
            return state.map(p => piece(p, action));
            break;
        default:
            //a reducer MUST return something
            return state;
    }
};

export const dice = (state = Immutable.List([]), action) => {
    switch (action.type) {
        case GAME_ACTIONS.ROLL_DICE:
            return action.diceValues;
        default:
            return state;
    }
};
