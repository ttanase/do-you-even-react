var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watch = require('gulp-watch');
var gutil = require('gulp-util');
var browserify = require('browserify');
var babel = require('gulp-babel');

// gulp.task('clean', function() {
//     return gulp.src('build/**/*.*')
//         .pipe(clean());
// });

gulp.task('transform', function() {
    return gulp.src('./public/javascripts/src/**/*.js')
        .pipe(babel({
            presets: [
                "react",
                "es2015"
            ]
        }))
        .pipe(gulp.dest('./public/javascripts/build'));
});


gulp.task('js', ['transform'], function() {
    // Assumes a file has been transformed from
    // ./app/src/main.jsx to ./app/dist/main.js
    return browserify('./public/javascripts/build/main.js')
        .bundle()
        .on('error', gutil.log)
        .pipe(source('main.js'))
        .pipe(buffer())
        .pipe(gulp.dest('./public/javascripts/dist'))
});

gulp.task('default', ['js'], function() {
    return gulp.watch('./public/javascripts/src/**/*.js', ['js']);
});